#!/bin/bash
#-------------------------------------------------------------------------------------------
# Copy and analyze ssh & vpn logs on Raspberry Pi.
#-------------------------------------------------------------------------------------------

function copyAndChangePermission {
    cp -v "$1"/"$2" .
    chmod u+r "$2"
}

copyAndChangePermission "/var/log/" "auth.log"
copyAndChangePermission "/var/log/" "openvpn-status.log"
copyAndChangePermission "/var/log/" "openvpn.log"

#Jan 24 03:17:01 raspberrypi4 CRON[16068]: pam_unix(cron:session): session opened for user root by (uid=0)
#Jan 24 03:17:01 raspberrypi4 CRON[16068]: pam_unix(cron:session): session closed for user root
cat auth.log | \
    grep -v -ae "[A-Z][a-z]\{2\} [ 0-9]\{2\} [0-9]\{2\}:17:0[12] raspberrypi4 CRON\[[0-9]\{3,5\}\]: pam_unix(cron:session): session [openedcls]\{6\} for user root" | \
    tee auth_woCRON.log

#
#nrLogRej=$(cat auth.log | grep -a -c "Connection closed by" || true)
#echo "Number of rejected connections: ${nrLogRej}"
#
#cat auth.log || true

#if [ 0 -eq ${nrLogRej} ] ; then
#	exit 0
#else
#	exit -1
#fi

exit 0
